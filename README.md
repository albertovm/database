# Indice
- [Indice](#indice)
- [database.proyecto-becarios-cert-2019.ml.](#databaseproyecto-becarios-cert-2019ml)
  - [Instalación mysql](#instalaci%c3%b3n-mysql)
  - [Instalación de postgres](#instalaci%c3%b3n-de-postgres)
    - [phpPgadmin](#phppgadmin)
  - [Instalar VPN](#instalar-vpn)
    - [iniciando el servicio](#iniciando-el-servicio)
  - [Autenticación SSH](#autenticaci%c3%b3n-ssh)
  - [Reglas `iptables`](#reglas-iptables)

# database.proyecto-becarios-cert-2019.ml.

## Instalación  mysql
1. Instalar `wget`, para posteriormente bajar el repositorio. `sudo yum install
   wget`
2. Descargar el repositorio de la página `wget`
   https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
3. Autenticar el software descargado, comparando el valor del siguiente comando
   con el md5 que aparece en la página.
~~~
sudo md5sum mysql80-community-release-el7-3.noarch.rpm
~~~
4. Una vez autenticado, instalar el paquete.

~~~
sudo rpm -Uvh mysql80-community-release-el7-3.noarch.rpm
~~~
5. En este caso es requerida la versión 5.7, por lo que se deshabilita la
   versión 8.
~~~
sudo yum-config-manager —disable mysql80-community
sudo yum-config-manager —enable mysql57-community
~~~
6. Buscar los paquetes actuales.
~~~
sudo yum repolist enabled |grep mysql
~~~
7. Instalar MySQL.
~~~
sudo yum install mysql-community-server
~~~
8. Inicializar el servicio.
~~~
sudo systemctl start mysqld.service
~~~
9. Por default MySQL configura una contraseña de root temporal.  Ver esa
   contraseña pues posteriormente se utiliza.
~~~
sudo grep ‘temporary password’ /var/log/mysqld.log
~~~
10. Por sí solo el servicio de MySQL no se inicializa al reiniciar el servidor.
    Indicar, al servidor, que reinicie el servicio después de un reboot. sudo
    systemctl enable mysql
11. Comprobar que el comando previo ha sido aceptado. sudo systemctl is-enabled
    mysql La salida debe ser enabled.
12. Realizar la instalalción segura. mysql_secure_installation Para esto
    requerirá la contraseña root, ingresar la temporal. Agregar una nueva
    contraseña para root, remover usuarios anonimos, no permitir login en root
    remoto, remover las bases de datos de test y recargar las tablas de
    privilegios.
13. Ahora se puede iniciar sesión en MySQL con la nueva contraseña. ` mysql -u
    root -p `


## Instalación de postgres
1. Instalación de PostgresSQL v10
2. `rpm -Uvh` rpm -Uvh
   https://yum.postgresql.org/10/redhat/rhel-7-x86_64/pgdg-centos10-10-2.noarch.rpm
   
3. Instalar repositorio de postgres yum install postgresql10-server postgresql10
   
1. Instalar postgres /usr/pgsql-10/bin/postgresql-10-setup initdb 
1. Inicializar el servicio de la base de datos
2. Habilitar el servicio ` systemctl start postgresql-10.service `
1. Verificar la instalacion `su - postgres -c "psql" `
1. Cambiar la contrasena del usuario postgres
~~~
postgres=# \password postgres 
~~~

### phpPgadmin
![pgadmin-png](images/pgadmin.png)

## Instalar VPN
1. Instalar repositorio epel ` sudo yum update -y ` nos aeguramos de tener las
   ultimas versiones e instalamos ` sudo yum install epel-release -y `
2. Instalar OpenVpn, en general `openvpn` esta por defecto en `CentOS`, pero si
   queremos instalarlo podemos hacer ` sudo yum install openvpn -y `
3. Instalar rsa `sudo yum install easy-rsa -y `
   ### Configuración OpenVpn
podemos copiar el archivo de ejemplo de configuración de `openvpn` en nuestro
tenemos la version `openvpn-2.4.7`
~~~
sudo cp /usr/share/doc/openvpn-2.4.7/sample/sample-config-files/server.conf /etc/openvpn
~~~
ahora podemos editar el archivo `/etc/openvpn` teniendo en cuenta que los
comentarios se marcan `;`
- y procedemos a generar la llave de encriptación
~~~
sudo openvpn --genkey --secret /etc/openvpn/myvpn.tlsauth
~~~
crearemos un directorio `/etc/openvpn/easy-rsa` para guardar las llaves y los
certificados y dentro de el el archivo de sus variables
~~~
sudo nano /etc/openvpn/easy-rsa/vars
~~~
cargamos las variables `source ./vars` y ejecutamos los scripts `./clean-all` y 
`./build-ca`

### iniciando el servicio
~~~
sudo systemctl -f enable openvpn@server.service
~~~
activar el servicio
~~~
sudo systemctl start openvpn@server.service
~~~

## Autenticación SSH
1. Generar las llaves pública y privada `ssh-keygen`
2. Copiar la llave pública al equipo al que se requiera conectarse asegurandose
   que se tengan los permisos correctos, podemos usar el comando `scp`, en este
   casolas copiaremos en la ruta `~/.ssh/authorized_keys`, dependiendo del
   `home` de cada usuario

## Reglas `iptables`
Para ver las reglas de `iptables` ver el archivo `config/reglas_ip.sh`