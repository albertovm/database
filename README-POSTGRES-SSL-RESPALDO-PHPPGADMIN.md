># Instalación y configuración de ``Postgresql-10``

1. Instalar repositorio de postgres:
```
sudo rpm -Uvh https://yum.postgresql.org/10/redhat/rhel-7-x86_64/pgdg-centos10-10-2.noarch.rpm
```
2. Instalar postgres: 
```
sudo yum install postgresql10-server postgresql10
```
3. Inicializar la base de datos:
```
sudo postgres /usr/pgsql-10/bin/postgresql-10-setup initdb
```
4. Habilitar el servicio de postgres:
```
sudo systemctl start postgresql-10.service
```
- Para iniciar el servicio de postgres cada vez que incie el sistema ejecutar el comando ``sudo systemctl enable postgresql-10.service``
5. Verificar la instalación ingresando a postgres:
```
sudo su - postgres -c "psql"
```
- Debe aparecer el siguiente prompt:
 ```
(10.10)
Type "help" for help.
        
postgres=# 
```
>## Configuración del usuario ``postgres`` y equipo ``web``

1. Cambiar la contraseña del usuario postgres a ``contraseñaSecreta``:
```
postgres=# alter role postgres with encrypted password 'contraseñaSecreta';
```
2. Crear la base de datos ``tonejitosrm`` para el equipo de ``web``, así como su usuario ``postonejito`` y contraseña ``contraseñaSecreta``:
```
postgres=# create role postonejito with login encrypted password 'contraseñaSecreta' createdb;
postgres=# create database tonejitosrm with owner postonejito;
```
- La **contraseña** se mantiene en secreto para la documentación

>## Configuración del archivo ``pg_hba.conf`` 

1. Ingresar al archivo ``pg_hba.conf`` que se encuentra en la ruta ``sudo vim /var/lib/pgsql/10/data/pg_hba.conf`` para configurar las conexiones remotas permitidas a ``postgres``, el archivo se configuró de la siguiente manera para permitir las redes la la unam, dominios tonejito y equipo mail:
```
 configuration parameter, or via the -i or -h command line switches.

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    tonejitosrm     postonejito     18.189.132.180/8        md5             #Equipo web
host    all             all             tonejito.cf             md5
host    all             all             becarios.tonejito.cf    md5
host    all             all             priv.becarios.tonejito.cf            md5
host    all             all             132.248.0.0/16          md5             #Segmento UNAM
host    all             all             132.247.0.0/16          md5             #Segmento UNAM
host    all             all             127.0.0.1/32            md5
host    all             all             aruba.tonejito.info     md5
host    all             all             vultr.tonejito.info     md5
host    all             all             tonejito.ovh            md5
host    all             all             h1p.tonejito.info       md5
host    all             all             tonejito.org            md5
host    all             all             tonejito.info           md5
host    all             all             db.proyecto-becarios-cert-2019.ml md5
host    all             all             10.0.8.0/24             md5             #VPN

# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
```
- El segmento **10.0.8.0/24**  se configuró para la VPN
2. Reiniciar el servicio de postgres ``sudo systemctl restart postgresql-10.service``

>## Configuración del archivo ``postgresql.conf``

1. Ingresar al archivo ``postgresql.conf`` que se encuentra en la ruta ``sudo vim /var/lib/pgsql/10/data/postgresql.conf`` en la sección de conexiones y autenticación, el archivo se configuró de la siguiente manera:
```
#------------------------------------------------------------------------------
# CONNECTIONS AND AUTHENTICATION
#------------------------------------------------------------------------------

# - Connection Settings -

listen_addresses = '*'                  # what IP address(es) to listen on;

                                        # comma-separated list of addresses;
                                        # defaults to 'localhost'; use '*' for all
                                        # (change requires restart)
port = 5432                             # (change requires restart)
max_connections = 100                   # (change requires restart)
superuser_reserved_connections = 3      # (change requires restart)

```
2. Reiniciar el servicio de postgres ``sudo systemctl restart postgresql-10.service``

- Para iniciar sesión remota se debe ejecutar el comando ``psql -h [host] -U [usuario] -d [baseDatos] -W``

>## Instalación phpPgAdmin

1. Instalar el servidor apache e iniciarlo:
```
sudo yum install httpd
sudo systemctl start httpd.service
```
- Para iniciar el servicio de apache cada vez que inicie el sistema ejecutar el comando ``sudo systemctl enable httpd.service``

2. Instalar php
```
sudo yum install php php-pgsql
```

3. Instalar phpPgAdmin
```
sudo yum install phpPgAdmin
```

>### Configuración del archivo  ``phpPgAdmin.conf``

1. Ingresar al archivo ``phpPgAdmin.conf`` que se encuentra en la ruta ``sudo vim /etc/httpd/conf.d/phpPgAdmin.conf``, el archivo se configuró de la siguiente manera:

```
# This configuration file maps the phpPgAdmin directory into the URL space. 
# By default this application is only accessible from the local host.
#

Alias /phpPgAdmin /usr/share/phpPgAdmin

<Location /phpPgAdmin>
    <IfModule mod_authz_core.c>
        # Apache 2.4
        Require all granted
        #Require host example.com
    </IfModule>
    <IfModule !mod_authz_core.c>
        # Apache 2.2
        Order deny,allow
        Allow from all
        Allow from 127.0.0.1
        Allow from ::1
        # Allow from .example.com
    </IfModule>
</Location>
```

>### Configuración del archivo  ``config.inc.php``

1. Ingresar al archivo ``config.inc.php`` que se encuentra en la ruta ``sudo vim /etc/phpPgAdmin/config.inc.php``, se modificaron las siguientes líneas:
```
$conf['servers'][0]['host'] = 'localhost';
$conf['owned_only'] = true;
```

4. Permitir el acceso a phpPgAdmin ``sudo setsebool -P httpd_can_network_connect_db 1``
5. Reiniciar el servicio de apache y postgres para aplicar los cambios:
```
sudo systemctl restart httpd.service
sudo systemctl restart postgresql-10.service
```
6. Entrar al sitio de [phpPgAdmin](https://database.proyecto-becarios-cert-2019.ml/phpPgAdmin/)

>## Configuración SSL

1. Crear el certificado y llave privada del certificado para la base de datos
```
sudo openssl req -new -x509 -days 365 -nodes -text -out server.crt -keyout server.key -subj "/CN=db.proyecto-becarios-cert-2019.ml"
```
2. Cambiar los permisos a la llave privada con el comando ``sudo chmod og-rwx server.key``
3. Mover el archivo ``server.crt`` a la ruta ``sudo mv server.ctr /etc/ssl/certs/server.ctr``, cambiarle el dueño y grupo ``sudo chown root:ssl-cert`` y por último cambiarle los permisos ``sudo chmod 0644 server.ctr``
4. Mover el archivo ``server.key`` a la ruta ``sudo mv server.key/etc/ssl/private/server.key``, cambiarle el dueño y grupo ``sudo chown root:ssl-cert`` y por último cambiarle los permisos ``sudo chmod 0640 server.key``

>### Configuración del archivo ``postgresql.conf``

1. Ingresar al archivo ``postgresql.conf`` que se encuentra en la ruta ``sudo vim /var/lib/pgsql/10/data/postgresql.conf`` en la sección de ssl, de descomentaron las siguientes líneas y se hizo referencia al certificado y su llave de la siguiente manera:

```
ssl = on
ssl_prefer_server_ciphers = on
ssl_cert_file = '/etc/ssl/certs/server.crt'
ssl_key_file = '/etc/ssl/private/server.key'
password_encryption = md5               # md5 or scram-sha-256
```
>### Configuración del archivo ``pg_hba.conf`` 

1. Ingresar al archivo ``pg_hba.conf`` que se encuentra en la ruta ``sudo vim /var/lib/pgsql/10/data/pg_hba.conf`` para agregar la conexion a ``postgres``, el archivo se configuró de la siguiente manera para permitir la conexión ssl:
```
hostssl    all    all    0.0.0.0/0    md5
```
6. Reiniciar el servicio de postgres para aplicar los cambios ``sudo systemctl restart postgresql-10.service``
7. Realizar una conexión de la base de datos con el comando ``psql -h [host] -U [usuario] -d [baseDatos]``
- Se muestra lo siguiente:
```
psql (10.10)
SSL connection (protocol: TLSv1.2, cipher: ECDHE-RSA-AES256-GCM-SHA384, bits: 256, compression: off)
Type "help" for help.

postgres=# 
```
>## Monitoreo con Nagios

1. En el prompt de postgres ``sudo su - postgres -c "psql"`` se creo el usuario nagios con permisos de lectura y su base de datos para que se pudiera monitorear la base de datos:
```
postgres=# create role nagios with login encrypted password 'contraseñaSecreta' createdb;
postgres=# create database tonejitosrm with owner nagios
postgres=# grant connect on database nagios to nagios;
postgres=# grant usage on schema public to nagios;
postgres=# grant select on all sequences in schema public to nagios;
postgres=# grant select on all tables in schema public to nagios;
```
- La **contraseña** se mantiene en secreto para la documentación
>### Configuración del archivo ``pg_hba.conf`` 

1. Ingresar al archivo ``pg_hba.conf`` que se encuentra en la ruta ``sudo vim /var/lib/pgsql/10/data/pg_hba.conf`` para agregar la conexion a ``postgres``, el archivo se configuró de la siguiente manera para permitir la conexión ssl:
```
host    all             nagios          nagios.proyecto-becarios-cert-2019.ml.  md5     #Nagios
host    all             nagios          104.156.225.38/8        md5             #Nagios

```
2. Reiniciar el servicio de postgres para aplicar los cambios ``sudo systemctl restart postgresql-10.service``

>## Respaldo de la base de datos
1. En el prompt de postgres ``sudo su - postgres -c "psql"`` se creo el usuario respaldos con permisos de superusuario para que se pudiera realizar el respaldo de la base de datos:
```
postgres=# create role respaldos with login encrypted password 'contraseñaSecreta' superuser;
```
>### Configuración del archivo ``pg_hba.conf`` 

1. Ingresar al archivo ``pg_hba.conf`` que se encuentra en la ruta ``sudo vim /var/lib/pgsql/10/data/pg_hba.conf`` para agregar la conexion a ``postgres``, el archivo se configuró de la siguiente manera para permitir la conexión ssl:
```
host    all             respaldos       13.59.79.168/8          md5             #Equipo storage
host    all             respaldos       localhost               md5
host    replication     respaldos       13.59.79.168/8          md5
host    replication     respaldos       10.0.8.0/24             md5
```
- El segmento **10.0.8.0/24**  se configuró para la VPN
2. Se creo al usuario ``respaldos`` dentro del servidor de ``database`` con ``sudo useradd respaldos --password contraseñaSecreta`` además de darle permisos de ``sudo`` en su ``home`` en el archivo ``sudo visudo``:
```
respaldos ALL=(ALL) ALL
respaldos ALL =/home/respaldos
```
- La **contraseña** se mantiene en secreto para la documentación
3. Para el respaldo se necesitó crear los siguientes dos archivos en el home del usuario ``respaldos`` para que no se le solicitara su contraseña al momento de crear el respaldo de la base de datos:
- Para postgres:
```
sudo vim .pgpass
```
- Se configuró de la siguiente manera:
```
hostname:port:database:username:password
```
- Para mysql:
```
sudo vim .my.cnf
```
- Se configuró de la siguiente manera:
```
[mysql]
user=respaldos
password="contraseñaSecreta"

[mysqldump]
user=respaldos
password="contraseñaSecreta"

[mysqlimport]
user=respaldos
password="contraseñaSecreta"

[mysqlclient]
user=respaldos
password="contraseñaSecreta"

[mysqldiff]
user=respaldos
password="contraseñaSecreta"

```
- La **contraseña** se mantiene en secreto para la documentación
4. Se acordó con el equipo ``storage`` que los respaldos se iba a realizar mediante un comando en el archivo ``sudo vim /etc/crontab``:
```
10 * * * * respaldos pg_dumpall -U respaldos -p 5432 -h database.proyecto-becarios-cert-2019.ml > /home/respaldos/postgres/postgres.backup
10 * * * * respaldos mysqldump -u respaldos --all-databases > /home/respaldos/mysql/mysql.backup
```
- Los comandos se ejecutan cada 10 minutos
5. Reiniciar el cron ``sudo systemctl restart crond.service``
- Para iniciar el cron cada vez que inicie el sistema ``sudo systemctl enable crond.service``