#!/bin/sh
iptables -F
iptables -X
iptables -Z
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -A OUTPUT -j ACCEPT
iptables -A FORWARD -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p udp --dport 80 -j ACCEPT
iptables -A INPUT -p udp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -p udp --dport 443 -j ACCEPT
iptables -A INPUT -s 104.156.225.38 -j ACCEPT
iptables -A INPUT -s 189.203.29.215 -j ACCEPT
iptables -A INPUT -s 189.225.31.240 -j ACCEPT
iptables -A INPUT -s 187.199.179.161 -j ACCEPT
iptables -A INPUT -s 189.242.1.231 -j ACCEPT
iptables -A INPUT -s 189.216.121.155 -j ACCEPT
iptables -A INPUT -s tonejito.cf -j ACCEPT
iptables -A INPUT -s becarios-tonejito.cf -j ACCEPT
iptables -A INPUT -s priv.becarios.tonejito.cf -j ACCEPT
iptables -A INPUT -s aruba.tonejito.info -j ACCEPT
iptables -A INPUT -s vultr.tonejito.info -j ACCEPT
iptables -A INPUT -s ovh.tonejito.info -j ACCEPT
iptables -A INPUT -s tonejito.ovh -j ACCEPT
iptables -A INPUT -s h1p.tonejito.info -j ACCEPT
iptables -A INPUT -s tonejito.org -j ACCEPT
iptables -A INPUT -s tonejito.info -j ACCEPT
iptables -A INPUT -s nagios.proyecto-becarios-cert-2019.ml. -j ACCEPT
iptables -A INPUT -s 132.247.0.0/16 -j ACCEPT
iptables -A INPUT -s 132.248.0.0/16 -j ACCEPT
iptables -A INPUT -s 10.0.8.0/24 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
#mysql
iptables -A INPUT -s 18.189.174.158 -p tcp --dport 3306 -j ACCEPT
iptables -A INPUT -s 13.59.79.168 -p tcp --dport 3306 -j ACCEPT
iptables -A INPUT -s 18.189.132.180 -p tcp --dport 3306 -j ACCEPT
iptables -A INPUT -s 18.189.227.64 -p tcp --dport 3306 -j ACCEPT
iptables -A INPUT -s 3.16.142.205 -p tcp --dport 3306 -j ACCEPT
#postgresql
iptables -A INPUT -s 18.189.174.158 -p tcp --dport 5432 -j ACCEPT
iptables -A INPUT -s 13.59.79.168 -p tcp --dport 5432 -j ACCEPT
iptables -A INPUT -s 18.189.132.180 -p tcp --dport 5432 -j ACCEPT
iptables -A INPUT -s 18.189.227.64 -p tcp --dport 5432 -j ACCEPT
iptables -A INPUT -s 3.16.142.205 -p tcp --dport 5432 -j ACCEPT
# cerrar todo lo demas
iptables -A INPUT -p tcp --dport 1:102 -j DROP
iptables -A INPUT -p udp --dport 1:1024 -j DROP
iptables -A INPUT -p tcp --dport 3306 -j DROP
iptables -A INPUT -p tcp --dport 10000 -j DROP
iptables -A INPUT -p udp --dport 10000 -j DROP